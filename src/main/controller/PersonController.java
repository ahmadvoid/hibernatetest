package main.controller;

import main.models.Person;
import main.utils.HibernateUtilPerson;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import java.util.List;

public class PersonController {
    // Method to CREATE an Person in the Database

    private static SessionFactory factory = null;

    public PersonController() {

        try {
            factory = HibernateUtilPerson.getSessionFactory();
        }
        catch (Throwable ex) {
            System.err.println("Failed to create Session Factory object." + ex);
            throw new ExceptionInInitializerError(ex);
        }
    }

    public Integer AddPerson(String firstName, String lastName, String email, String phoneNumber){

        Session session = factory.openSession();
        Integer employeeID = null;

        try {
            session.beginTransaction();

            Person p1 = new Person();
            p1.setFirstName(firstName);
            p1.setLastName(lastName);
            p1.setEmail(email);
            p1.setPhoneNumber(phoneNumber);

            employeeID = (Integer) session.save(p1);

            session.getTransaction().commit();
        }
        catch (HibernateException ex) {
            ex.printStackTrace();
        }
        finally {
            session.close();
        }
        return employeeID;
    }

    public Integer AddPerson(Person person){

        Session session = factory.openSession();
        Integer employeeID = null;

        try {
            session.beginTransaction();

            Person p1 = new Person();
            p1.setFirstName(person.getFirstName());
            p1.setLastName(person.getLastName());
            p1.setEmail(person.getEmail());
            p1.setPhoneNumber(person.getPhoneNumber());

            employeeID = (Integer) session.save(p1);

            session.getTransaction().commit();
        }
        catch (HibernateException ex) {
            ex.printStackTrace();
        }
        finally {
            session.close();
        }
        return employeeID;
    }

    // Method to  READ all the employees
    public List GetAllPersons( ){
        Session session = factory.openSession();
        List persons = null;
        try {
            session.beginTransaction();
            persons = session.createQuery("FROM Person").list();

            session.getTransaction().commit();
        }
        catch (HibernateException e) {
            e.printStackTrace();
        }
        finally {
            session.close();
        }

        return persons;
    }


}
