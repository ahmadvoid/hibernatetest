package main;

import main.controller.PersonController;
import main.models.Person;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class App {

    private static String InputWhileNotNull(String enterMessage) {
        String Input = "";
        while (Input.equals("")) {
            System.out.println(enterMessage);
            Input = new Scanner(System.in).next();
        }
        return Input;
    }

    public static Person personInput(){
        Person person = new Person();

        person.setFirstName(InputWhileNotNull("Enter Person First Name:"));
        person.setLastName(InputWhileNotNull("Enter Person Last Name:"));
        person.setPhoneNumber(InputWhileNotNull("Enter Person Phone Number:"));
        person.setEmail(InputWhileNotNull("Enter Person Email:"));

        return person;
    }

    public static void main(String[] args){
        PersonController personController = new PersonController();

        while (true){
            System.out.println(
                    "Choose an option:\n" +
                            "1- Add New Person\n" +
                            "2- Get All Persons\n" +
                            "3- Exit"
            );

            char userInput = new Scanner(System.in).next().charAt(0);

            if(userInput == '1') {
                personController.AddPerson(personInput());
                System.out.println("Person Successfully Added");
            }
            else if (userInput == '2') {
                List personList = personController.GetAllPersons();

                for(Object p1: personList){
                    System.out.println(p1.toString());
                }
            }
            else if(userInput == '3'){
                System.out.println("Thank U For Using Our Program ..^^");
                break;
            }
            else {
                System.out.println("Wrong Option!!\nTry Again");
            }
        }
    }
}
