package main.utils;

import java.util.Properties;

import main.config.HibernateConfigureProperties;
import main.models.Person;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class HibernateUtilPerson {

    private static SessionFactory buildSessionFactory() {
        try {

            Properties properties = HibernateConfigureProperties.getProperties();

            return new Configuration()
                    .setProperties(properties)
                    .addAnnotatedClass(Person.class)
                    .buildSessionFactory();

        } catch (Throwable ex) {
            System.err.println("build Session Factory failed :" + ex);
            throw new ExceptionInInitializerError(ex);
        }
    }

    public static SessionFactory getSessionFactory() {
        return buildSessionFactory();
    }

    public static void close() {
        // Close all cached and active connection pools
        getSessionFactory().close();
    }
}
