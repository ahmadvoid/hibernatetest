package main.config;

import main.models.Person;
import org.hibernate.cfg.Configuration;
import org.hibernate.cfg.Environment;

import java.util.Properties;
import java.util.Scanner;

public final class HibernateConfigureProperties {

    private static Properties properties = null;

    public static Properties getProperties(){
        if(properties == null){
            createProperties();
        }
        return properties;
    }

    private static String InputWhileNotNull(String enterMessage) {
        String Input = "";
        while (Input.equals("")) {
            System.out.println(enterMessage);
            Input = new Scanner(System.in).next();
        }
        return Input;
    }

    private static void TestingConnection() {

        try {
            new Configuration()
                    .setProperties(properties)
                    .addAnnotatedClass(Person.class)
                    .buildSessionFactory();

            System.out.println("Successfully Connected\n\n");
        }
        catch (Exception ex){
            System.out.println
            (
                    "Error Details Entered!!" +
                    "\n" +
                    "Try Again" +
                    "\n\n" +
                    "=========================" +
                    "\n\n"
            );
            ex.printStackTrace();
            createProperties();
        }
    }

    private static void createProperties(){
        Properties properties = new Properties();

        properties.put(Environment.DRIVER, "com.microsoft.sqlserver.jdbc.SQLServerDriver");
        properties.put(Environment.FORMAT_SQL, "true");
        properties.put(Environment.DIALECT, "org.hibernate.dialect.SQLServer2008Dialect");
        properties.put(Environment.HBM2DDL_AUTO, "create");

        String sqlURL = InputWhileNotNull("Enter SQL URL");
        System.out.println("=========================");
        String DatabaseName = InputWhileNotNull("Enter Database Name");
        System.out.println("=========================");
        String isIntegratedSecurity = InputWhileNotNull("Is Integrated Security?\nAnswere with true || false");
        System.out.println("=========================");

        properties.put(Environment.URL,sqlURL + ";databaseName="+ DatabaseName + ";integratedSecurity" + isIntegratedSecurity+";");

        if(!isIntegratedSecurity.equals( "true"))
        {
            properties.put(Environment.USER, InputWhileNotNull("Enter Username:"));
            System.out.println("=========================");
            properties.put(Environment.PASS, InputWhileNotNull("Enter Password:"));
            System.out.println("=========================");
        }

        System.out.println("All Done.. \n\nTesting Connection..");

            HibernateConfigureProperties.properties = properties;
        TestingConnection();

    }
}
