package main.models;

import javax.persistence.*;

@Entity
@Table(name = "tbl_Persons")
public class Person {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "PersonID")
    private int Id;

    @Column(name = "FirstName")
    private String firstName;

    @Column(name = "LastName")
    private String lastName;

    @Column(name = "Email")
    private String email;

    @Column(name = "PhoneNumber")
    private String PhoneNumber;

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return PhoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        PhoneNumber = phoneNumber;
    }

    @Override
    public String toString() {
        return
                "Person Details:" +
                        "\n" +
                        "[" +
                        "\n" +
                            " ID = " + this.Id +
                            " First Name = " + this.firstName + "\n" +
                            " Last Name = " + this.lastName + "\n" +
                            " Email = " + this.email + "\n" +
                            " Phone Number = " + this.PhoneNumber + "\n" +
                        "]" +
                        "\n\n" +
                        "======================" +
                        "\n\n";
    }
}
